/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TIMO;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * Program name: TIMO (Toiminnaltaan Itellaa Muistuttava Ohjelmisto)
 * FXMLPackageViewController.java 
 * @author Eetu
 * 
 */

/*
In this view:
- User can create a package
- User can make new object or choose from 4 example objects
- User choose start and end SmartPosts from comboboxes. Using city combobox
sorts SmartPost combobox by showing city related SmartPosts first.
- User choose Packageclass and can get info about different classes
- TextArea shows instructions for users
*/
public class FXMLPackageViewController implements Initializable {
    @FXML
    private Button backButton;
    @FXML
    private Button createButton;
    @FXML
    private ComboBox<Object> objectBox;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField weightField;
    @FXML
    private CheckBox fragileCheckBox;
    @FXML
    private RadioButton thirdClass;
    @FXML
    private RadioButton secondClass;
    @FXML
    private RadioButton firstClass;
    @FXML
    private Button infoButton;
    @FXML
    private TextArea infoTextArea;
    @FXML
    private ComboBox<String> startLocationBox;
    @FXML
    private ComboBox<SmartPost> startPostBox;
    @FXML
    private ComboBox<String> endLocationBox;
    @FXML
    private ComboBox<SmartPost> endPostBox;
    @FXML
    private WebView webView;
    @FXML
    private ToggleGroup radioGroup;
   
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        //index.html is needed to calculate distances between SmartPost to check later
        //if 1.class packet can be done (distance max. 150). webWiev in unvisible for 
        //user
        webView.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        //Creates 4 predefined objects and adds them to object combobox
        objectBox.getItems().add(new Object("Uusi esine", null, 0, false));
        objectBox.getItems().add(new Object("Marmoripatsas", "100*70*60", 6, true));
        objectBox.getItems().add(new Object("Raamattu", "30*20*10", 0.5, false));
        objectBox.getItems().add(new Object("Aalto malja", "30*20*20", 0.3, true));
        objectBox.getItems().add(new Object("Jääkaappi", "150*70*60", 20, false));          

    }    

    //Returns to StartView when user press back button
    @FXML
    private void backButtonAction(ActionEvent event) {
        Stage stage = (Stage) backButton.getScene().getWindow();
        stage.close();
    }

    //Gets user inputs for package creation and creates a package. Contains a lot 
    //checks for user inputs and checks also if package requirements are fullfilled.
    @FXML
    private void createPackageAction(ActionEvent event) {
        
        //User inputs are gathered first. These are done with separate methods.
        Object object = getObject();
        int packageClass = getPackageClass();
        SmartPost start = getStartLocation();
        SmartPost end = getEndLocation();
        
        //Checks that all required inputs are done to create a package
        if(object != null && packageClass != 0 && start != null && end != null) {                       
            
            //Creates ArrayList of starting and ending geopoints in from
            //[start latitude, start longitude, end latitude, end longitude]
            //This format is needed to run JavaScript command createPath
            ArrayList<String> geoData = new ArrayList();
            geoData.add(start.getGeoPoint().getLatitude());
            geoData.add(start.getGeoPoint().getLongitude());
            geoData.add(end.getGeoPoint().getLatitude());
            geoData.add(end.getGeoPoint().getLongitude());
            
            //Calculates distance between starting and ending point
            double distance = Double.parseDouble(webView.getEngine().executeScript(
                    "document.createPath(" + geoData + ",'red',1)").toString());

            String size = object.getSize();           
            
            //transfers size dimensions String from form xx*xx*xx to 
            //String Array [xx, xx, xx] and again to int Array [int, int, int]
            //This must be done to check if object size is suitable for Packages
            String[] strArray = size.split("\\*");
            int[] intArray = new int[strArray.length];
            for(int i = 0; i < strArray.length; i++) {
                intArray[i] = Integer.parseInt(strArray[i]);
            }
            
            //This code is run if user selected first class package from radiobuttons
            if(packageClass == 1){
                
                //Checks if delivery distance is < 150 km and that object is not
                //fragile. First class package can be created and added to warehouse
                //if these requirements are fullfilled. Otherwise package cant be 
                //created and user is informed about it.
                if(distance <= 150 && object.getFragile() != true) {
                    FirstClass packet = new FirstClass(object, geoData, distance, 
                            packageClass, start.getPostOffice(), end.getPostOffice());
                    Storage.getInstance().addPackage(packet);
                    infoTextArea.setText("1. luokan paketin luominen onnistui.");
                }
                
                //informs user that distance is too long and object is fragile
                else if(distance > 150 && object.getFragile() == true) {
                    infoTextArea.setText("Pakettia ei hyväksytä toimitettavaksi:\n"
                            + "* Välimatka " + distance + " km (max. 150 km)\n "
                            + "* Ei hyväksytä särkyviä tuotteita.");
                }
                
                //informs user that delivery distance is too long
                else if(distance > 150 && object.getFragile() != true) {
                    infoTextArea.setText("Pakettia ei hyväksytä toimitettavaksi:\n"
                            + "* Välimatka " + distance + " km ylittää sallitun "
                            + "rajan 150 km.");
                }
                
                //informs user that object is fragile
                else if(distance < 150 && object.getFragile() == true) {
                    infoTextArea.setText("Pakettia ei hyväksytä toimitettavaksi:\n"
                            + "* Ei hyväksytä särkyviä tuotteita.");
                }        
            }    
            
            //This code is run if user selected second class package from radiobuttons
            if(packageClass == 2){
                
                //Checks if object size is <= 50*50*50 and weight is <= 5 kg.
                //Second class package can be created and added to warehouse
                //if these requirements are fullfilled. Otherwise package cant be 
                //created and user is informed about it. 
                if(intArray[0] <= 50 && intArray[1] <= 50 && intArray[2] <= 50 &&
                        object.getWeight() <= 5) {
                    SecondClass packet = new SecondClass(object, geoData, distance,
                            packageClass, start.getPostOffice(), end.getPostOffice());
                    Storage.getInstance().addPackage(packet);
                    infoTextArea.setText("2. luokan paketin luominen onnistui.");
                }
                
                //informs user that object size and weight is too big
                else if((intArray[0] > 50 || intArray[1] > 50 || intArray[2] > 50) &&
                        object.getWeight() > 5) {
                    infoTextArea.setText("Pakettia ei hyväksytä toimitettavaksi:\n"
                            + "* Paketin koko " + object.getSize() + "cm (max. 50*50*50 cm)\n"
                            + "* Paketin paino " + object.getWeight() + " kg (max. 5 kg)");
                }
                
                //informs user that object weight is too big
                else if(intArray[0] < 50 && intArray[1] < 50 && intArray[2] < 50 &&
                        object.getWeight() > 5) {
                    infoTextArea.setText("Pakettia ei hyväksytä toimitettavaksi:\n"
                            + "* Paketin paino " + object.getWeight() + " kg (max. 5 kg)");
                }
                
                //informs user that object size is too big
                else if((intArray[0] > 50 || intArray[1] > 50 || intArray[2] > 50) &&
                        object.getWeight() <= 5) {
                    infoTextArea.setText("Pakettia ei hyväksytä toimitettavaksi:\n"
                            + "* Paketin koko " + object.getSize() + "cm (max. 50*50*50 cm)\n");
                }
            }
                       
            //This code is run if user selected third class package from radiobuttons
            if(packageClass == 3){
                
                //Checks if object size is > 50*50*50 and weight is > 5 kg.
                //Third class package can be created and added to warehouse
                //if these requirements are fullfilled. Otherwise package cant be 
                //created and user is informed about it.
                if(intArray[0] > 50 && intArray[1] > 50 && intArray[2] > 50 &&
                        object.getWeight() > 5) {
                    ThirdClass packet = new ThirdClass(object, geoData, distance,
                            packageClass, start.getPostOffice(), end.getPostOffice());
                    Storage.getInstance().addPackage(packet);
                    infoTextArea.setText("3. luokan paketin luominen onnistui.");
                }
                
                //Informs user that object size and weight is too small
                else if((intArray[0] <= 50 || intArray[1] <= 50 || intArray[2] <= 50) 
                        && object.getWeight() <= 5) {
                    infoTextArea.setText("Pakettia ei hyväksytä toimitettavaksi:\n"
                            + "* Paketin koko " + object.getSize() + "cm (Oltava > 50*50*50 cm)\n"
                            + "* Paketin paino " + object.getWeight() + " kg (Oltava > 5 kg)");
                }
                
                //Informs user that object weight is too small
                else if(intArray[0] > 50 && intArray[1] > 50 && intArray[2] > 50 &&
                        object.getWeight() <= 5) {
                    infoTextArea.setText("Pakettia ei hyväksytä toimitettavaksi:\n"
                            + "* Paketin paino " + object.getWeight() 
                            + " kg (Oltava > 5 kg)");
                }
                
                //informs user that object size is too small
                else if((intArray[0] <= 50 || intArray[1] <= 50 || intArray[2] <= 50) && object.getWeight() > 5) {
                    infoTextArea.setText("Pakettia ei hyväksytä toimitettavaksi:\n"
                            + "* Paketin koko " + object.getSize() + "cm (oltava > 50*50*50 cm)\n");
                }                          
            }
        }                                       
    }

    //User get info about features and requirements of different package classes
    //by pressing info button
    @FXML
    private void infoButtonAction(ActionEvent event) {
        infoTextArea.setText("1. luokan paketti:\n* Nopein kuljetus\n"
                + "* Lähetysmatka max. 150 km\n* Ei särkyville tuotteille\n\n"
                + "2. luokan paketti:\n* Lähetysmatka rajaton\n"
                + "* Esineen koko max. 50cm*50cm*50cm\n* Esineen paino max. 5 kg\n\n"
                + "* 3. luokan paketti:\n* Isoille kestäville tavaroille\n"
                + "* Esineen koko min. 50cm*50cm*50cm\n* Esineen paino min. 5 kg\n"
                + "* Hitain kuljetus");
    }

    //When starting city is selected on combobox it sorts starting 
    //SmartPosts combobox by moving SmartPosts related to that city at the 
    //beginning of the combobox
    @FXML
    private void chooseStartLocationAction(ActionEvent event) {  
 
        String start = startLocationBox.getValue(); 
        
        ArrayList<SmartPost> smartPosts = new ArrayList();
        ArrayList<SmartPost> sortedList = new ArrayList();
        
        //gets added SmartPosts from combobox to a ArrayList
        for(int i = 0; i < startPostBox.getItems().size(); i++) {
            smartPosts.add(startPostBox.getItems().get(i));            
        }       
        
        //clears SmartPosts from combobox and adds sorted SmartPosts to it
        startPostBox.getItems().clear();
        
        for (SmartPost sp : smartPosts) {
            if(start.matches(sp.getCity())) {
                sortedList.add(0, sp);
            }
            else {
                sortedList.add(sp);
            }
        }
        startPostBox.getItems().addAll(sortedList);
              
    }    

    //When ending city is selected on combobox it sorts ending 
    //SmartPosts combobox by moving SmartPosts related to that city at the 
    //beginning of the combobox
    @FXML
    private void chooseEndLocationAction(ActionEvent event) {
        String end = endLocationBox.getValue(); 
        
        ArrayList<SmartPost> smartPosts = new ArrayList();
        ArrayList<SmartPost> sortedList = new ArrayList();
        
        //gets added SmartPosts from combobox to a ArrayList
        for(int i = 0; i < endPostBox.getItems().size(); i++) {
            smartPosts.add(endPostBox.getItems().get(i));            
        }       
        
        //clears SmartPosts from combobox and adds sorted SmartPosts to it
        endPostBox.getItems().clear();
        
        for (SmartPost sp : smartPosts) {
            if(end.matches(sp.getCity())) {
                sortedList.add(0, sp);
            }
            else {
                sortedList.add(sp);
            }
        }
        endPostBox.getItems().addAll(sortedList);
    }
    
    //Gets user input for package class from radiobuttons and returns it 
    //to createPackageAction. Informs user if radiobutton not selected.
    //radiobuttons are on same radiogroup so only one can be selected at a time
    public int getPackageClass() {        
        int packageClass = 0;
                
        if(firstClass.isSelected()) {
            packageClass = 1;            
        }
        else if(secondClass.isSelected()) {
            packageClass = 2;            
        }
        else if(thirdClass.isSelected()) {
            packageClass = 3;           
        }        
        
        else {
            infoTextArea.setText("Huom ! Valitse pakettiluokka\n");          
        }
        return packageClass;
    }
    
    //Gets user input for object to be packaged. User can select predefined objects
    //from combobox or choose to make a new object. Object is returned to
    //createPackageAction
    public Object getObject() { 
        
        Object object = null;
        
        //Checks that object or option to create new one is selected and informs
        //user if not
        if(objectBox.getValue() != null) {
            
            //This code is run if user selected create new object
            if(objectBox.getValue().getName().equals("Uusi esine") == true) {            
            
                //Checks that all required inputs to create a object are done.
                //If not, user is informed about missing input
                if(nameField.getText().isEmpty() == false && 
                        sizeField.getText().isEmpty() == false && 
                        weightField.getText().isEmpty() == false){
                    
                    //Checks that size input is form cm*cm*cm and weight is a number
                    //using regular expressions. If yes, object is created. If
                    //not, user is informed of wrong input.
                    if(sizeField.getText().matches("\\d+\\*\\d+\\*\\d+") && 
                            weightField.getText().matches("\\d+(\\.\\d+)?")) {
                        
                        object = new Object(nameField.getText(), sizeField.getText(),
                                Double.parseDouble(weightField.getText()),
                                fragileCheckBox.isSelected());
                    }
                    else{
                        infoTextArea.setText("Virheellisest tuotetiedot:\n"
                            + "* Tuotteen mitat muodossa cm*cm*cm\n"
                            + "* Paino oltava numero");
                    }
                }
                //creates errorText about missing inputs
                else {
                    String errorText = "Huom! Virhe tuotteen luomisessa:\n";

                    if(nameField.getText().isEmpty() == true) {
                       errorText = errorText + "* Syötä tuotteen nimi\n";

                    }

                    if(sizeField.getText().isEmpty() == true) {
                        errorText = errorText + "* Syötä esineen koko\n";
                    }

                    if(weightField.getText().isEmpty() == true) {
                        errorText = errorText + "* Syötä tuotteen paino\n";
                    }

                    infoTextArea.setText(errorText);           
                }                                                         
            }
            
            //This code is run if user selected predefined object
            else {                    
                object = objectBox.getValue();
            }
        }
        else {
            infoTextArea.setText("Valitse tavara tai luo uusi");
        }
        
        return object;        
    }
    
    //Gets user input for starting SmartPost and returns it to createPackageAction
    //Informs user if input is missing or start and end locations are the same
    public SmartPost getStartLocation() {
        SmartPost sp = startPostBox.getValue();
        if(sp == null) {
            infoTextArea.setText("* Valitse lähtösijainti\n"); 
        }
        else if(sp.equals(endPostBox.getValue())) {
            infoTextArea.setText("* Lähtö- ja loppusijainti eivät voi olla samat");
        }
        return sp;              
    }
  
    //Gets user input for ending SmartPost and returns it to createPackageAction
    //Informs user if input is missing
    public SmartPost getEndLocation() {
        SmartPost sp = endPostBox.getValue();
        if(sp == null) {
            infoTextArea.setText("* Valitse loppusijainti\n");       
        }
        return sp;     
    }
    
    //This method is run when FXMLPackageView is opened. This updates
    //city and Smartpost comboboxes (starting and ending) with SmartPosts and cities
    //that have been added to the map in StartView
    public void updateCitiesAndSmartPosts(ArrayList<SmartPost> smartPosts, ArrayList<String> cities) {
        startLocationBox.getItems().addAll(cities);
        endLocationBox.getItems().addAll(cities);
        
        startPostBox.getItems().addAll(smartPosts);
        endPostBox.getItems().addAll(smartPosts);       
        
    }
}
