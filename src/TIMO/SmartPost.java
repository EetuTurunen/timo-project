/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TIMO;

/**
 * Program name: TIMO (Toiminnaltaan Itellaa Muistuttava Ohjelmisto)
 * SmartPost.java 
 * @author Eetu
 * 
 */

/* 
SmartPosts are created in Databuilder using open Finnish Smartpost XML -data
and stored in ArrayList in DataBuilder. Geopoint is used to locate Smartpost on map.
String variables are used to show information about Smartpost when selected on map
*/

public class SmartPost {
    private String postCode;
    private String city;
    private String address;
    private String availability;
    private String postOffice;
    private GeoPoint gp;
    
    public SmartPost(String pc, String c, String a, String av, String po, String lat, String lng) {
        postCode = pc;
        city = c;
        address = a;
        availability = av;
        postOffice = po;
        gp = new GeoPoint(lat, lng);       
    }
    
    public String getPostCode() {
        return postCode;
    }

    public String getCity() {
        return city;
    }

    public String getAddress() {
        return address;
    }

    public String getAvailability() {
        return availability;
    }

    public String getPostOffice() {
        return postOffice;
    }

    public GeoPoint getGeoPoint() {
        return gp;
    }
    
    //This is done to make combobox options more informative
    @Override
    public String toString() {
        return postOffice;
    }   
}
