/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TIMO;

import java.util.ArrayList;

/**
 * Program name: TIMO (Toiminnaltaan Itellaa Muistuttava Ohjelmisto)
 * Storage.java 
 * @author Eetu
 * 
 */

/*
Storage class is done by Singleton-pattern so only one storage instance can be done
Storage keeps track of Packages in stock and Packages that have been delivered
with two different ArrayLists. It also counts all packages created.
*/
public class Storage {
    private static Storage storage = null;
    private int packageCount;
    private String name;
    private ArrayList<Package> storedPackages;
    private ArrayList<Package> deliveredPackages;
    
    private Storage() {
        name = "Postipalvelun varasto";
        packageCount = 0;
        storedPackages = new ArrayList();
        deliveredPackages = new ArrayList();
    }
    
    //only one instance of storage can made
    public static Storage getInstance() {
        if(storage == null) {
            storage = new Storage();
        } 
        return storage;    
    }
    
    public String getName() {
        return name;
    }
    
    //used to get stored Packages for Combobox in FXMLStartViewController and
    //to create statistics in FXMLStatisticsViewController
    public ArrayList<Package> getStoredPackages() {
        return storedPackages;
    }
    
    //used to create statistics in FXMLStatisticsController
    public ArrayList<Package> getDeliveredPackages() {
        return deliveredPackages;
    }
    
    //used to show number of packages created in FXMlStatisticsViewController
    public int getPackageCount() {
        return packageCount;
    }
    
    //This adds created Packages to warehouse and keeps increases total packages
    //created
    public void addPackage(Package p) {
        storedPackages.add(p);
        packageCount++;
    }    
    
    //This changes package from in stock -state to delivered
    //Adds selected package to deliveredPackages and deletes it from warehouse
    //(storedPackages) with index
    public void sendPackage(Package p, int i){
        deliveredPackages.add(p);
        deletePackage(i);
    
    }
    
    //Deletes package from warehouse with known index
    public void deletePackage(int index) {
        storedPackages.remove(index);
                     
    }
    
    
}

