/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TIMO;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * Program name: TIMO (Toiminnaltaan Itellaa Muistuttava Ohjelmisto)
 * FXMLStatisticsViewController.java 
 * @author Eetu
 * 
 */

/*
In this view:
- User can see total number of Packages created
- User get information of stored and delivered Packages (Package class, object,
departure office, arrival office, delivery distance)
- Create a .txt file containing these same informations
*/
public class FXMLStatisticsViewController implements Initializable {
    @FXML
    private TextArea storedTextArea;
    @FXML
    private Button returnButton;
    @FXML
    private TextArea deliveredTextArea;
    @FXML
    private Button saveButton;
    @FXML
    private Label packageCountLabel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Storage storage = Storage.getInstance();
        
        //shows total number of packages created
        packageCountLabel.setText("" + storage.getPackageCount());
 
        //Gets information of packages in warehouse from storage and creates String 
        //containing information of packages to be shown on textField
        ArrayList<Package> storedPackages = storage.getStoredPackages();    
        String storageData = "";
        
        //Informs that there is no packages in warehouse
        if(storedPackages.isEmpty()) {
            storageData = "Varastossa ei ole paketteja";
        }
        
        //Shows information of packages in warehouse
        else {
            for (Package p : storedPackages) {
                storageData = storageData + "Paketin luokka: " + p.getPackageClass() + 
                        ". luokka" + "    Tavara: " + p.getObject() + "    Lähtöpaikka: " 
                        + p.getStartOffice() + "    Saapumispaikka: " + p.getEndOffice() 
                        + "    Toimitusmatka: " + p.getDistance() + " km\n";
            }
        }
        
        storedTextArea.setText(storageData);
        
        //Gets information of delivered packages from Storage and creates String 
        //containing information of packages to be shown on textField
        ArrayList<Package> deliveredPackages = storage.getDeliveredPackages();
        String deliveryData = "";
        
        //Informs that there is no packages that have been delivered
        if(deliveredPackages.isEmpty()) {
            deliveryData = "Yhtään pakettia ei ole lähetetty";
        }
        
        //Shows information of packages delivered
        else {
            for (Package p : deliveredPackages) {
                deliveryData = deliveryData + "Paketin luokka: " + p.getPackageClass() + 
                        ". luokka" + "    Tavara: " + p.getObject() + "    Lähtöpaikka: " 
                        + p.getStartOffice() + "    Saapumispaikka: " + p.getEndOffice() 
                        + "    Toimitusmatka: " + p.getDistance() + " km\n";
            }
        }
        
        deliveredTextArea.setText(deliveryData);
    }    
    
    //Close window 
    @FXML
    private void returnAction(ActionEvent event) {
        Stage stage = (Stage) returnButton.getScene().getWindow();
        stage.close();       
    }

    //Creates PackageData.txt file to project folder with same information shown 
    //to user in StatisticsView
    @FXML
    private void saveDataAction(ActionEvent event) throws IOException {
        
        BufferedWriter out = new BufferedWriter(new FileWriter("PackageData.txt"));  
        
        String Data = Storage.getInstance().getName() + " tiedot:" 
                + System.lineSeparator() + System.lineSeparator() +
                "Paketteja luotu yhteensä: " + Storage.getInstance().getPackageCount()
                + System.lineSeparator() + System.lineSeparator() +
                "Lähetetyt paketit:" + System.lineSeparator();
        
        //Package information needs to be split to separate Strings and add one
        //by one to Data String with working line breaks
        String[] strArray1 = deliveredTextArea.getText().split("\n");
        
        for(int i = 0; i < strArray1.length; i++) {
                Data = Data + strArray1[i] + System.lineSeparator();
            }
        
        Data = Data + System.lineSeparator() + "Varastossa olevat paketit:" 
                + System.lineSeparator();
        
        //Package information needs to be split to separate Strings and add one
        //by one to Data String with working line breaks
        String[] strArray2 = storedTextArea.getText().split("\n"); 
        
        for(int i = 0; i < strArray2.length; i++) {
                Data = Data + strArray2[i] + System.lineSeparator();
            }
        
        out.write(Data); 
                  
        out.close();               
    
    }
    
}
