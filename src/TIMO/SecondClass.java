/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TIMO;

import java.util.ArrayList;

/**
 * Program name: TIMO (Toiminnaltaan Itellaa Muistuttava Ohjelmisto)
 * SecondClass.java 
 * @author Eetu
 * 
 */

/*
Secondclass package is created using Package constructor
Secondclass package features and requirements:
- Average delivery speed
- No distance limit
- Object can be fragile
- Size limit max. 50cm*50cm*50cm 
- Weight limit max. 5 kg
*/

public class SecondClass extends Package {
    
    public SecondClass(Object o, ArrayList gd, double d, int c, String so, String eo){
        super (o, gd, d, c, so, eo);
    }
    
}
