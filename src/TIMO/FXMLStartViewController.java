/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TIMO;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * Program name: TIMO (Toiminnaltaan Itellaa Muistuttava Ohjelmisto)
 * FXMLStartViewController.java 
 * @author Eetu
 * 
 */

/*
In this view:
- User can see the map
- Add Smartposts to map
- Open Package creation view
- Update combobox that show packages in warehouse
- Send created Packages by selecting Package and color from comboboxes. Delivery
progress is drawn on map. 
- Remove delivery tracks shown on map
- Open statistics view to check stored and delivered Packages
- Choose to End program
- TextArea shows information in some actions
*/

public class FXMLStartViewController implements Initializable {
    @FXML
    private WebView mapView;
    @FXML
    private Button sendButton;
    @FXML
    private ComboBox<Package> storageBox;
    @FXML
    private Button updateButton;
    @FXML
    private Button addButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button createButton;
    @FXML
    private ComboBox<String> cityBox;
    @FXML
    private ComboBox<String> colorBox;
    @FXML
    private TextArea infoTextArea;
    @FXML
    private Button openStatisticsButton;
    @FXML
    private Button endButton;

    
    private DataBuilder db = null;
    private ArrayList<SmartPost> addedSmartPosts = new ArrayList();
    private ArrayList<String> addedCities = new ArrayList();

    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //loads index.html from project files. Doing this shows Finnish map
        //on mapView and makes it able for user to use JavaScript commands 
        //interactwith the map
        mapView.getEngine().load(getClass().getResource("index.html").toExternalForm()); 
        
        //Creates SmartPosts in DataBuilder
        db = new DataBuilder();       
               
        ArrayList<String> cities = db.getCities();
        
        //initialize comboboxes 
        cityBox.getItems().addAll(cities);  
        colorBox.getItems().addAll("Punainen", "Keltainen", "Vihreä", "Musta", "Valkoinen", "Sininen");       
    }    

    //User selects city on combobox and SmartPosts in that city is added on map
    @FXML
    private void addSmartPostAction(ActionEvent event) {
        
        ArrayList<SmartPost> smartposts = db.getSmartPosts();
        String city = cityBox.getValue();
        
        if(city != null) { //checks that city is selected by user
            
            for(SmartPost sp : smartposts) {
                
                //checks that smartPost is not already added on the map
                //and adds only SmartPosts that belong to city selected
                if(sp.getCity().matches(city) && addedSmartPosts.contains(sp) == false) {
                    
                    //JavaScript command to add SmartPost on map
                    mapView.getEngine().executeScript("document.goToLocation('" 
                            + sp.getAddress() + ", " + sp.getPostCode() + " " 
                            + sp.getCity() + "', '" + sp.getPostOffice() 
                            + ": " + sp.getAvailability() + "', 'blue')");
                    
                    //keeps track of added SmartPosts
                    addedSmartPosts.add(sp);
                    
                    infoTextArea.setText("Valitsemasi paikkakunnan "
                            + "Smartpost-automaatit lisätty");
                    
                    //Keeps track of cities which SmartPosts have been added
                    if(addedCities.contains(sp.getCity()) != true) {
                        addedCities.add(sp.getCity());
                    }
                }
                
                else if(sp.getCity().matches(city) && addedSmartPosts.contains(sp) == true){
                    infoTextArea.setText("Valitsemasi paikkakunnan SmartPost-automaatit olivat jo lisättynä kartalle");
                }
            }
        }
        
        else {
            infoTextArea.setText("Valitse ensin paikkakunta, jonka SmartPost-automaatit haluat lisätä");
        }
    }

    //Opens Package creation view and transfers added SmartPosts and Cities to
    //comboboxes in next view
    @FXML
    private void createPackageAction(ActionEvent event) {
        
        try {
            Stage packageView = new Stage();
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLPackageView.fxml"));
            
            Parent page = (Parent) loader.load();
                       
            FXMLPackageViewController controller = loader.<FXMLPackageViewController>getController();
            controller.updateCitiesAndSmartPosts(addedSmartPosts, addedCities);
                        
            Scene scene = new Scene(page);
            
            packageView.setScene(scene);
            packageView.show();
            
            } catch (IOException ex) {
            Logger.getLogger(FXMLStartViewController.class.getName()).log(Level.SEVERE, null, ex);
        }                
    }
    
    
    @FXML
    //User can send package that is in warehouse by selecting package and
    //color for the path shown on the map for delivery. Sent package is removed
    //from warehouse and combobox. User gets delivery information when starting
    //to send a package.
    private void sendPackageAction(ActionEvent event) {
        Package delivery = storageBox.getValue();
        int index = storageBox.getItems().indexOf(delivery);
        
        //gets user input for delivery color to be drawn
        String color = getColor();
        
        //Checks that package and color is selected. Otherwise informs user of 
        //missing inputs
        if(delivery != null && color != null) {
            //Path is drawn on map using JavaScript command createPath which
            //needs Packet geoData (ArrayList of start and ending locations), 
            //color and package class (changes delivery speed)
            mapView.getEngine().executeScript("document.createPath(" + delivery.geoData 
                    + ", '"+ color + "', " + delivery.getPackageClass() + ")");
            
            //Gives user information related to package delivered
            infoTextArea.setText("Paketin lähettäminen onnistui! Lähetyksen tiedot:\n"
                    + "* Paketin luokka: " + delivery.getPackageClass() + ". luokka\n"
                    + "* Lähetetty tavara: " + delivery.getObject() + "\n"                   
                    + "* Lähtöpaikka: " + delivery.getStartOffice() + "\n"
                    + "* Kohde: " + delivery.getEndOffice() + "\n"
                    + "* Toimitusmatka: " + delivery.getDistance() + "km\n"
                    + "Tavara poistettu varastosta");
            
            //Informs storage that package have been sent
            Storage.getInstance().sendPackage(delivery, index);
            
            //Updates Package combobox (removes sent package)
            storageBox.getItems().clear();
            storageBox.getItems().addAll(Storage.getInstance().getStoredPackages());
        }
        else {
            infoTextArea.setText("Paketin lähettämisessä tapahtui virhe:\n"
                    + "* Tarkista onko paketti ja väri valittu");
        }
        
    }

    //removes all drawn paths on the map using JavaScprit command deletePaths
    @FXML
    private void deletePathsAction(ActionEvent event) {
        mapView.getEngine().executeScript("document.deletePaths()");
    }
    
    //User can update package combobox (packages in warehouse)
    //after creating new packages
    @FXML
    private void updatePackageAction(ActionEvent event) {
        storageBox.getItems().clear();
        storageBox.getItems().addAll(Storage.getInstance().getStoredPackages());
        
    }    
    
    //returns user input for color to sendPackageAction
    public String getColor() {
        String choice = colorBox.getValue();
        String color = null;
        
        //returns selected color to sendPackageAction in english, so JavaScript
        //command createPath can use it
        if(choice != null) {
        
            if(choice.matches("Punainen")) {
               color = "red";
            }
            else if(choice.matches("Musta")) {
               color = "black";
            }
            else if(choice.matches("Keltainen")) {
               color = "yellow";
            }
            else if(choice.matches("Vihreä")) {
               color = "green";
            }
            else if(choice.matches("Valkoinen")) {
               color = "white";
            } 
            else if(choice.matches("Sininen")) {
               color = "blue";
            }       
        }
        
        return color;
    }

    //Closes window 
    @FXML
    private void endProgramAction(ActionEvent event) {
        Stage stage = (Stage) endButton.getScene().getWindow();
        stage.close();
    }

    //Opens statisticsView where user can see information about:
    //- Total number of packages created
    //- Information of stored and delivered packages
    @FXML
    private void openStatisticsAction(ActionEvent event) {
        
        try {
            Stage statisticsView = new Stage();
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLStatisticsView.fxml"));
            
            Parent page = (Parent) loader.load();
                        
            Scene scene = new Scene(page);
            
            statisticsView.setScene(scene);
            statisticsView.show();
            
            } catch (IOException ex) {
            Logger.getLogger(FXMLStartViewController.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }
}
