/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TIMO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Program name: TIMO (Toiminnaltaan Itellaa Muistuttava Ohjelmisto)
 * DataBuilder.java 
 * @author Eetu
 * 
 */

/*
Databuilder gathers XML-data to string from URL, converts it to XML format 
and parses data for SmartPosts from it. SmartPosts and cities are stored in ArrayLists
which are used in FXMLStartViewController.
 */

public class DataBuilder {
    private Document doc;
    private String content;
    private ArrayList<SmartPost> smartpost_array = new ArrayList();
    private ArrayList<String> city_array = new ArrayList();
    
    public DataBuilder() {
        content = getXML("http://smartpost.ee/fi_apt.xml");
        Document xml = toXML(content);
        parseSmartPostData(xml);
    }
    
    //reads XML-data from URL and saves it in content String
    public String getXML(String u) {
        try {
            URL url = new URL(u);
            
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            
            String inputLine;
            content = "";
            
            while((inputLine = br.readLine()) != null) {
                content += inputLine + "\n";
            }
            br.close();
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return content;
    }
    
    //Converts data in content String to XML-form
    public Document toXML (String content) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
                 
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(DataBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }       
        return doc;
    }  
    
    //Parses required data to create SmartPosts from XML-form doc
    //and adds them to ArrayList. Then it adds every city once to other ArrayList
    public void parseSmartPostData(Document d) {
        doc = d;
        NodeList nodes = doc.getElementsByTagName("place");
        
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            
            String pc = getValue("code", e);
            String c = getValue("city", e);
            String a = getValue("address", e);
            String av = getValue("availability", e);
            String po = getValue("postoffice", e);
            String lat = getValue("lat", e);
            String lng = getValue("lng", e);
            
            smartpost_array.add(new SmartPost(pc, c, a, av, po, lat, lng));            
        } 
        
        for(SmartPost sp : smartpost_array) {
            if(city_array.contains(sp.getCity())) {
                continue;
            }
            else {
                city_array.add(sp.getCity());
            }            
        } 
        
    }
    
    //This method is used in parsing Smarposts to get wanted data from XML
    public String getValue(String tag, Element e) {
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
    
    //This is used in FXMLStartViewController when adding SmartPosts to the map 
    public ArrayList<SmartPost> getSmartPosts() {
        return smartpost_array;
    }
    
    //In StartViewController user can choose a city and add all SmartPosts in 
    //that city to the map.
    public ArrayList<String> getCities() {
        return city_array;
    }
    
    
}
