/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TIMO;

import java.util.ArrayList;

/**
 * Program name: TIMO (Toiminnaltaan Itellaa Muistuttava Ohjelmisto)
 * FirstClass.java 
 * @author Eetu
 * 
 */

/*
Firstclass package is created using Package constructor
Firstclass package features and requirements:
- Fastest delivery
- Distance max. 150 km
- Object cant be fragile because they always break
- No limit for size and weight
*/
public class FirstClass extends Package {
    
    public FirstClass(Object o, ArrayList gd, double d, int c, String so, String eo){
        super (o, gd, d, c, so, eo);
    }
           
}
