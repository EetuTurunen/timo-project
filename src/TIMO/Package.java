/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TIMO;

import java.util.ArrayList;

/**
 * Program name: TIMO (Toiminnaltaan Itellaa Muistuttava Ohjelmisto)
 * Package.java 
 * @author Eetu
 * 
 */

/*
There is 3 different types of packages and they have different requirements 
for objects and distances when creating a Package
- FirstClass
- SecondClass
- ThirdClass
Package is abstract and these 3 types extends Package. Package includes:
- one object,
- starting and ending coordinates in geoData ArrayList (JavaScript command uses this kind of input)
- distance between coordinates
- Names of offices at start and at the end
- Packageclass to determine delivery speed and to create right Package

*/

public abstract class Package {
    protected Object object;
    protected ArrayList<String> geoData;
    protected double distance;
    protected int packageClass;
    protected String startOffice;
    protected String endOffice;
    
    public Package(Object o, ArrayList<String> gd, double d, int s, String so, String eo) {
        object = o;
        geoData = gd;
        distance = d;
        packageClass = s;
        startOffice = so;
        endOffice = eo;
    }
    
    public Object getObject() {
        return object;
    }

    public ArrayList<String> getGeoData() {
        return geoData;
    }
    
    public double getDistance() {
        return distance;
    }    
    
    public int getPackageClass() {
        return packageClass;
    }
    
    public String getStartOffice() {
        return startOffice;
    }
    
    public String getEndOffice() {
        return endOffice;
    }
    
    //This is done to make combobox options more informative
    @Override
    public String toString() {
        return packageClass + ". luokka: " + object.getName();
    }
       
}
