/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TIMO;

import java.util.ArrayList;

/**
 * Program name: TIMO (Toiminnaltaan Itellaa Muistuttava Ohjelmisto)
 * ThirdClass.java 
 * @author Eetu
 * 
 */

/*
Thirdclass package is created using Package constructor
Thirdclass package features and requirements:
- Slowest delivery speed
- No distance limit
- Object can be fragile
- Size must be bigger than 50cm*50cm*50cm 
- Weight must be more than 5 kg
*/

public class ThirdClass extends Package {
    
    public ThirdClass(Object o, ArrayList gd, double d, int c, String so, String eo){
        super (o, gd, d, c, so, eo);
    }
    
}
