/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TIMO;

/**
 * Program name: TIMO (Toiminnaltaan Itellaa Muistuttava Ohjelmisto)
 * Object.java 
 * @author Eetu
 * 
 */

/*
Theres always one Object in Package. Variables size, weight and fragile determines
in which Package Classes (1.,2. or 3.) they can be delivered. 
*/
public class Object {
    private String name;
    private String size;
    private double weight;
    private boolean fragile;
    
    public Object (String n, String s, double w, boolean f) {
        name = n;
        size = s;
        weight = w; 
        fragile = f;
    }

    public String getSize() {
        return size;
    }

    public double getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }
    
    public boolean getFragile() {
        return fragile;
    }
    
    //This is done to make combobox options more informative
    @Override
    public String toString() {
        return name;
    }

}
