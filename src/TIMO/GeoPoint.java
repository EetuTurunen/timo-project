/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TIMO;

/**
 * Program name: TIMO (Toiminnaltaan Itellaa Muistuttava Ohjelmisto)
 * GeoPoint.java 
 * @author Eetu
 * 
 */

/* 
There is always one GeoPoint for each SmartPost. 
GeoPoint is used in JavaScript commands:
- to show smartPost visually on the map
- when sending packages between SmartPosts
*/

public class GeoPoint {
    private String latitude;
    private String longitude;
    
    public GeoPoint(String lat, String lng) {
        latitude = lat;
        longitude = lng;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
    
}
